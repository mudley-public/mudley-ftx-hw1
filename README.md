# Mudley FTX

## Prerequisite

### Install Python 3.8.3

Download python from:

https://www.python.org/downloads/

For Windows, follow the instruction below:

https://medium.com/@itylergarrett.tag/how-to-install-python-3-7-on-windows-10-pc-the-non-developer-version-b063e1913b39

#### Check Python version

For Windows:

```shell
C:/Users/[CURRENT-USER]/AppData/Local/Programs/Python/Python38-[32 or 64]/python.exe --version
```

For Mac OS X:

```shell
python3 --version
```

#### Check PIP version

For Windows:

```shell
C:/Users/[CURRENT-USER]/AppData/Local/Programs/Python/Python38-[32 or 64]/Scripts/pip3.exe --version
```

For Mac OS X:

```shell
pip3 --version
```

### Install Git

For Windows:

Download git from:

https://gitforwindows.org/

For Max OS X:

Download git from:

https://git-scm.com/download/mac

### Register to Gitlab

### Clone source code

Go to workspace and run the following command in terminal:

```shell
https://gitlab.com/mudley-public/mudley-ftx-hw1.git
```

then enter Gitlab username and password

### Install Visual Studio Code IDE

Download Visual Studio Code from:

https://code.visualstudio.com/

### Install Python Package

#### Install package requests

For Windows:

```shell
C:/Users/[CURRENT-USER]/AppData/Local/Programs/Python/Python38-[32 or 64]/Scripts/pip3.exe install requests
```

For Mac OS X:

```shell
pip3 install requests
```

#### Install package python-dotenv

For Windows:

```shell
C:/Users/[CURRENT-USER]/AppData/Local/Programs/Python/Python38-[32 or 64]/Scripts/pip3.exe install python-dotenv
```

For Mac OS X:

```shell
pip3 install python-dotenv
```

### Configure api key and api secret

- Rename file .env.example to .env
- Update apiKey and apiSecret in .env file

### Run python file in terminal

For Windows:

```shell
C:/Users/[CURRENT-USER]/AppData/Local/Programs/Python/Python38-[32 or 64]/python.exe path/to/projec/mudley-ftx/ftx/order-place.py
```

Example:

```shell
C:/Users/samsung/AppData/Local/Programs/Python/Python38-[32 or 64]/python.exe c:/Users/samsung/workspace/nta/python/mudley-ftx/ftx/order-place.py
```

For Mac OS X:

```shell
python3 ftx/order-place.py
```
