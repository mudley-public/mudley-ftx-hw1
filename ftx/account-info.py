import os
import time
import hmac
import requests
from dotenv import load_dotenv
import json
from datetime import datetime, timezone

# load environment variable(s) from .env
load_dotenv()

# get environment variable(s) for apiKey and apiSecret
apiKey = os.getenv('apiKey', '')
apiSecret = os.getenv('apiSecret', '')


ts = int(time.time() * 1000)
payload = f'{ts}GET/api/account'.encode()
signature = hmac.new(apiSecret.encode(), payload, 'sha256').hexdigest()

# prepare http headers
headers = {
    # required headers
    'FTX-KEY': apiKey,
    'FTX-SIGN': signature,
    'FTX-TS': str(ts)

    # # optional header
    # 'Content-Type': 'application/json',
    # 'Accept': 'application/json',
}


# request rest api
response = requests.get('https://ftx.com/api/account', headers=headers)

print(response.status_code)
print(response.json())
