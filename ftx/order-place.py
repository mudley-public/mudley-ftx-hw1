import os
import time
import hmac
import requests
from dotenv import load_dotenv
import json
from datetime import datetime, timezone

# load environment variable(s) from .env
load_dotenv()

# get environment variable(s) for apiKey and apiSecret
apiKey = os.getenv('apiKey', '')
apiSecret = os.getenv('apiSecret', '')

method = 'POST'
path = '/api/orders'
ts = int(time.time() * 1000)

# prepare data
data = {
    "market": "BTC/USD",
    "side": "buy",
    "type": "market",
    "size": 1,
    "price": 10
}

payload = f'{ts}{method}{path}{json.dumps(data)}'.encode()
signature = hmac.new(apiSecret.encode(), payload, 'sha256').hexdigest()

print('payload:', payload)
print('signature:', signature)

# prepare http headers
headers = {
    # required headers
    'FTX-KEY': apiKey,
    'FTX-SIGN': signature,
    'FTX-TS': str(ts)

    # # optional header
    # 'Content-Type': 'application/json',
    # 'Accept': 'application/json',
}


uri = f'https://ftx.com{path}'
print('uri:', uri)

# request rest api
response = requests.post(uri, json=data, headers=headers)

print(response.status_code)
print(response.json())
