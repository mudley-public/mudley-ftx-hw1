import requests

# prepare http headers
# headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

# request rest api
# response = requests.get('https://httpbin.org/get', headers=headers)
# or
response = requests.get('https://httpbin.org/get')

# print response
print(response.status_code)
print(response.json())

